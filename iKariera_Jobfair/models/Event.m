//
//  Event.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/28.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "Event.h"

@implementation Event

@dynamic timeStamp;
@dynamic items;

- (NSArray*)sortedItems
{
    // すべてのアイテムの取得
    NSArray*    items;
    items = [self.items allObjects];
    
    // アイテムをソートする
    NSSortDescriptor*   descriptor;
    descriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:YES];
    
    return [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
}

@end
