//
//  Company.h
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/29.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Company : NSManagedObject

@property(nonatomic,retain) NSString* guide;
@property(nonatomic,retain) NSString* id;
@property(nonatomic,retain) NSString* name;
@property(nonatomic,retain) NSString* stand;

@end
