//
//  JobfairCoreData.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/07.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "Jobfair.h"

@implementation Jobfair

@dynamic city;
@dynamic country;
@dynamic dateBegin;
@dynamic dateEnd;
@dynamic description;
@dynamic id;
@dynamic name;
@dynamic place;
@dynamic street;
@dynamic exhibitor;
@dynamic link;
@dynamic map;
@dynamic presentationRoom;

@end
