//
//  JobfairCoreData.h
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/07.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Jobfair : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
//@property (nonatomic, retain) NSString * dateBegin;
//@property (nonatomic, retain) NSString * dateEnd;
@property (nonatomic, retain) NSDate * dateBegin;
@property (nonatomic, retain) NSDate * dateEnd;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * description;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * listId;
@property (nonatomic, retain) NSString * jobfairId;
@property (nonatomic, retain) NSString * activate;
@property (nonatomic, retain) NSString * exhibitor;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * map;
@property (nonatomic, retain) NSString * presentationRoom;


@end

