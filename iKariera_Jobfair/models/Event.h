//
//  Event.h
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/28.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//


// This is test to try to get Coredata structure.

#import <CoreData/CoreData.h>

@interface Event : NSManagedObject

@property(nonatomic,retain) NSString* timeStamp;
@property (nonatomic, retain) NSSet* items;
@property (nonatomic, readonly) NSArray* sortedItems;

@end