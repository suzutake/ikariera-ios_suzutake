//
//  NetworkConnector.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/04.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "NetworkConnector.h"

@implementation NetworkConnector

{
    id _delegate;
    NSMutableData *_receivedData;
    NSStringEncoding _encoding;
}

@synthesize receivedString = _receivedString;


- (id)initWithDelegate:(id)delegate
{
    self = [super init];
    if ( self )
    {
        _delegate = delegate;
    }
    return self;
}

/*
 * network connection
 */

-(BOOL)checkNWStatus
{
    Reachability *curReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    switch (netStatus) {
        case NotReachable:
            NSLog(@"Internet is not available now");
            return false;
            break;
            
        default:
            return true;
            break;
    }
}



// need authentication pattern. below method isn't used this application.
/*
- (void)openUrl:(NSURL *)url:(NSData *)postData
{
    _receivedData = [[NSMutableData alloc] init];
    if ( _receivedString )
    {
        _receivedString = nil;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if ( ! connection )
    {
        NSLog(@"NSURLConnection error");
    }
}
 */
- (void)openUrl:(NSURL *)url
{
    // check Network status.
    //[self checkNWStatus]:connected is true, disconnected is false;
    //NSLog(@"network status is %@",[self checkNWStatus]? @"YES":@"NO");
    
    _receivedData = [[NSMutableData alloc] init];
    if ( _receivedString )
    {
        _receivedString = nil;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    // below need try catch structure. If NSURLConnection failed, it do nothing.
    // so NSLog erro is meanless.    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if ( ! connection )
    {
        NSLog(@"NSURLConnection error");
    }
}

/*
 * we need following 4 delegate methods.
 ・connection:didReceiveResponse:（レスポンス受信時に1回のみ）
 ・connection:didReceiveData:（コンテンツデータ受信時。断片的なデータを通信が終了するまで何度も）
 ・connection:didFailWithError:when error occurs
 ・connectionDidFinishLoading:（通信完了時に1回のみ）
 */

/*
 * receive response
 */
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response
{
    NSLog(@"expectedContentLength:%lld", [response expectedContentLength]);
    NSLog(@"MIMTType:%@", [response MIMEType]);
    NSLog(@"suggestedFieldname:%@", [response suggestedFilename]);
    NSLog(@"textEncodingName:%@", [response textEncodingName]);
    NSLog(@"URL:%@", [response URL]);
    NSLog(@"statusCode:%d", [response statusCode]);
    NSLog(@"localizedStringForStatusCode:%@", [NSHTTPURLResponse localizedStringForStatusCode:[response statusCode]]);
    
    if ( [response statusCode] != 200 )
    {
        NSLog(@"network is something wrong.");
        [connection cancel];
        [self connection:connection didFailWithError:nil];
        //[_delegate performSelector:@selector(receiveFailed:) withObject:self];
        return;
    }
    
    
    NSDictionary *dict = [response allHeaderFields];
    NSArray *allKeys = [dict allKeys];
    int count = [allKeys count];
    NSString *key;
    NSString *value;
    for ( int i = 0; i < count; i++ )
    {
        key = [allKeys objectAtIndex:i];
        value = (NSString *)[dict valueForKey:key];
        NSLog(@"header %@:%@", key, value);
    }
    
    //NSArray *allCookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:[NSURL URLWithString:@"http://www.sample.com"]];
    //NSLog(@"How many Cookies:%d", allCookies.count);
    //for ( NSHTTPCookie *cookie in allCookies )
    //{
    //    NSLog(@"Name:%@ Value:%@ Expire:%@", cookie.name, cookie.value, cookie.expiresDate);
    //}
    
    NSString *encodingName = [[response textEncodingName] lowercaseString];
    NSLog(@"encodingName:%@", encodingName);
    if ( [encodingName isEqualToString:@"utf-8"] || [encodingName isEqualToString:@"utf8"] || encodingName.length == 0 )
    {
        _encoding = NSUTF8StringEncoding;
    }
    else
    {
        _encoding = NSShiftJISStringEncoding;
    }
}

/*
 * receiving contents
 */
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // add received data
    [_receivedData appendData:data];
}

/*
 * error
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection failed. Error - %@ %d %@", [error domain], [error code], [error localizedDescription]);
    
    [_delegate performSelector:@selector(receiveFailed:) withObject:self];
}

/*
 * receive completed
 */
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // set data
    NSLog(@"_encoding:%d", _encoding);
    _receivedString = [[NSString alloc] initWithData:_receivedData encoding:_encoding];
    if ( [_receivedString length] == 0 )
    {
        NSLog(@"failed to Receive data");
        [_delegate performSelector:@selector(receiveFailed)];
    }
    else
    {
        //NSLog(@"receive Success! _receivedString:-%@-", _receivedString);
        NSLog(@"receive Success!");
        [_delegate performSelector:@selector(receiveSucceed:) withObject:self];
    }
}

@end
