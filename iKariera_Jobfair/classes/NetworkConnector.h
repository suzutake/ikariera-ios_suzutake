//
//  NetworkConnector.h
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/04.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Reachability.h"

@class Reachability;

@interface NetworkConnector : NSObject

@property (strong, nonatomic) NSString *receivedString;

- (id)initWithDelegate:(id)delegate;

- (void)openUrl:(NSURL *)url;

-(BOOL)checkNWStatus;


@end
