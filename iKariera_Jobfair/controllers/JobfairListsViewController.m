//
//  ikvMasterViewController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/08/15.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "JobfairListsViewController.h"
#import "JobfairDetailViewController.h"

#import "NetworkConnector.h"
#import "CoreDataController.h"

#import "Jobfair.h"


@interface JobfairListsViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation JobfairListsViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self._jobfairDictionary = @{@"jobfair":@[@"jobfair3",@"jobfair2"],@"detail":@[@"detail1",@"detail2"]};
    
    // set up spineer
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center=CGPointMake(160, 240);
    [self.view addSubview:self.spinner];
    
    
    
    // check network status
    NetworkConnector *nw = [[NetworkConnector alloc]init];    
    if([nw checkNWStatus])
    {
        // online
        [self testAsyncTask];
    }else
    {
        // offline
        UIAlertView *offlineAlert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Network is not available now." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [offlineAlert show];
    }
    
    // if you delete sqlite file, please use below to register 1 column.
    /*
    if([self tableView:self.tableView numberOfRowsInSection:0] == 0){
        [self insertNewObject:nil];
    }
     */
    
      

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];    
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Jobfair" inManagedObjectContext:context];
    [newManagedObject setValue:@"Jobfair!" forKey:@"name"];
    [newManagedObject setValue:@"1" forKey:@"id"];    
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}
 */

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [[self.fetchedResultsController sections] count];
    
    NSLog(@"fetchedResultsController section count is %u",[[self.fetchedResultsController sections] count]);
    //NSLog(@"fetchedResultsController count is %u",[[self.fetchedResultsController fetchedObjects]count]);
    
    //return 1;
    return [[self.fetchedResultsController sections]count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    //NSLog(@"sectionInfor is %u",[sectionInfo numberOfObjects]);
    //return [sectionInfo numberOfObjects];
    //return 1;
    
    // set tableview counts from
    NSLog(@"fetchedResultsController count is %u",[[self.fetchedResultsController fetchedObjects]count]);
    return [[self.fetchedResultsController fetchedObjects]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    //[self configureCell:cell atIndexPath:indexPath];
    //[self configureCell:cell atIndexPath:indexPath text:@"yahooo"];
    
    
    //[self configureCell:cell atIndexPath:indexPath DataSorce:jobfairArray];
    
    //NSDictionary* jobfairDictionary = @{@"jobfair":@[@"jobfair1",@"jobfair2"],@"detail":@[@"detail1",@"detail2"]};
    //self._jobfairDictionary = @{@"jobfair":@[@"jobfair3",@"jobfair2"],@"detail":@[@"detail1",@"detail2"]};
                                        
    [self configureCell:cell atIndexPath:indexPath DataSorceDictionary:self._jobfairDictionary];

    NSLog(@"the cell is %@",cell);
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        [[segue destinationViewController] setDetailItem:object];
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    //NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
    
    //cell.textLabel.text = @"iKariera Jobfair 2013 Summer";
    //cell.detailTextLabel.text = @"24th Aug - 28th Aug Prague";
    self.title = @"Jobfair Lists";
    
    cell.textLabel.text=[NSString stringWithFormat:@"test %u",indexPath.row];
    cell.detailTextLabel.text=@"detailTest";
    
    //if(_fetchedResultsController != nil){
    
    //NSManagedObject *object = [_fetchedResultsController objectAtIndexPath:indexPath];
    //cell.textLabel.text=[[object valueForKey:@"Jobfair"]description];
    //}
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath DataSorce:(NSArray *)array
{
    //NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
    
    //cell.textLabel.text = @"iKariera Jobfair 2013 Summer";
    //cell.detailTextLabel.text = @"24th Aug - 28th Aug Prague";
    self.title = @"Jobfair Lists";
    
    //cell.textLabel.text=[NSString stringWithFormat:@"test %@",string];
    //cell.detailTextLabel.text=@"detailTest";
    
    //NSArray* jobfairArray = [[NSArray alloc]initWithObjects:@"jobfair1",@"jobfair2", nil];
    cell.textLabel.text=[array objectAtIndex:indexPath.row];
    //cell.detailTextLabel.text=[jobfairArray objectAtIndex:indexPath.row];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath DataSorceDictionary:(NSDictionary *)dict
{
    //NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
    
    //cell.textLabel.text = @"iKariera Jobfair 2013 Summer";
    //cell.detailTextLabel.text = @"24th Aug - 28th Aug Prague";
    self.title = @"Jobfair Lists";
    
    //cell.textLabel.text=[NSString stringWithFormat:@"test %@",string];
    //cell.detailTextLabel.text=@"detailTest";
    
    //NSArray* jobfairArray = [[NSArray alloc]initWithObjects:@"jobfair1",@"jobfair2", nil];
    NSArray* jobfairArray = [dict valueForKey:@"jobfair"];
    NSLog(@"jobfairArray count is %u",jobfairArray.count);
    cell.textLabel.text=[jobfairArray objectAtIndex:indexPath.row];
    
    NSArray* dateBeginArray = [dict valueForKey:@"dateBegin"];
    NSArray* dateEndArray = [dict valueForKey:@"dateEnd"];
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@ - %@",[dateBeginArray objectAtIndex:indexPath.row],[dateEndArray objectAtIndex:indexPath.row]];
    //cell.detailTextLabel.text=[[dict valueForKey:@"detail"]objectAtIndex:indexPath.row];
    //cell.detailTextLabel.text=[jobfairArray objectAtIndex:indexPath.row];
}

/*
 * CoreData functions
 */
#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    NSLog(@"start fetchedResultsController");
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    //NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Jobfair" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    //NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Jobfair"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
   
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // check fetch result
    NSLog(@"Fetched result counts is %u",[aFetchedResultsController.fetchedObjects count]);//2
    
    //return _fetchedResultsController;
    return aFetchedResultsController;
}

/*
 * AsyncTask
 */
- (void)testAsyncTask
{
    //start spinner
    [self.spinner startAnimating];
    
    NetworkConnector *connector = [[NetworkConnector alloc] initWithDelegate:self];
    //NSURL *url = [NSURL URLWithString:@"http://94.23.170.172/www.ikariera-mobile.cz/json_job_fair"];
    NSURL *url = [NSURL URLWithString:@"http://www.ikariera.cz:8080/ikariera-jobfairs/api-rest-jobfairs?device=android&format=json&active=true"];
    [connector openUrl:url];
    
}

/*
 *************************************************
 * get JSON Data and parse it
 *************************************************
 */

- (void)receiveSucceed:(id)sender
{
    NSLog(@"start receiveSucceed");
    // parse JSON file
    NetworkConnector *connector = (NetworkConnector *)sender;
    NSData *parseData = [connector.receivedString dataUsingEncoding:NSUTF8StringEncoding];
    
    //NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:parseData options:NSJSONReadingAllowFragments error:NULL];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:parseData options:0 error:NULL];
    NSLog(@"start export jsondata");
    // downloaded json data is following.
    //NSLog(@"%@",jsonObject);
    
    
    //NSDictionary *nameDictDetail = [nameArray objectAtIndex:0];
    NSLog(@"************* parse JSON data ***************");
    // Jobfair.name
    NSArray *nameArray = [jsonObject valueForKey:@"name"];
    NSLog(@"jobfairName1 is %@",[nameArray objectAtIndex:0]);
    //NSLog(@"jobfairName2 is %@",[nameArray objectAtIndex:1]);
    // Jobfair.id
    NSArray* idArray = [jsonObject valueForKey:@"id"];
    NSLog(@"jobfairId1 is %@",[idArray objectAtIndex:0]);
    //NSLog(@"jobfairId2 is %@",[idArray objectAtIndex:1]);
    
    NSArray *dateBeginArray =[jsonObject valueForKey:@"dateBegin"];
    NSArray *dateEndArray =[jsonObject valueForKey:@"dateEnd"];
    
    //NSArray* companyNameArray=[jsonObject valueForKey:@"exhibitor"];
    NSLog(@"++++++");
    
    //how to get companyName is array index0(1/2 jobfair array) -> array(1/? exhibitor array) -> valueforkey
    //NSLog(@"The first companyName is %@",[[[companyNameArray objectAtIndex:0]objectAtIndex:0] valueForKey:@"companyName"]);
    //NSLog(@"The first companyid is %@",[[[companyNameArray objectAtIndex:0]objectAtIndex:0] valueForKey:@"id"]);
    
    
    [self saveParsedDataWithDictionary:jsonObject];
    
    
    // set parsed data to show on tableview
    NSLog(@"************* set Jobfair Data ***************");
    // hoge
    self._jobfairDictionary = @{@"jobfair":nameArray,@"detail":@[@"detail1",@"detail2"],@"dateBegin":dateBeginArray,@"dateEnd":dateEndArray};
    NSLog(@"try reload");
    [self.tableView reloadData];
    /*
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Jobfair" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Jobfair"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // check fetch result
    NSLog(@"Fetched result counts is %u",[aFetchedResultsController.fetchedObjects count]);//2
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:aFetchedResultsController.fetchedObjects waitUntilDone:NO];
    */
    //end spinner
    [self.spinner stopAnimating];
}

/*
 *************************************************
 * save parsed JSON data to CoreData database
 *************************************************
 */

/*
-(void)testCoreData
{
    CoreDataController *testcd =[[CoreDataController alloc]init];
    
    //below 2 codes is test that insert new data to Event table in Coredata.
    //[testcd insertNewEvent];
    //[testcd save];

}
 */

//
// when there is no contents or network is something wrong, receiveFailed will be executed.
//
- (void)receiveFailed:(id)sender
{
    NSLog(@"receive Failed.");
    [self.spinner stopAnimating];
}

-(void)saveParsedDataWithDictionary:(NSDictionary*)dictionary
{
    NSLog(@"start testCoreDataWithDictionary");
    CoreDataController *coredataController = [[CoreDataController alloc]init];
    // reset tables
    [coredataController deleteTableDataFromDatabase:@"Jobfair"];
    //[coredataController deleteTableDataFromDatabase:@"Company"];
    //[coredataController deleteTableDataFromDatabase:@"Event"];
    
    // insert data to tables
    NSLog(@"start insert Jobfair");
    //[coredataController insertNewEventWithDictionary:dictionary];
    [coredataController insertNewJobfairWithDictionary:dictionary];
    //[coredataController insertTestData];
    
    //NSArray* exhibitorArray=[dictionary valueForKey:@"exhibitor"];
    //[coredataController insertNewCompanyWithArray:exhibitorArray];
    
    
    // if you want to save data to CoreData, you should use below save method last. It's ok to call it 1 time.
    [coredataController save];
}



@end
