//
//  LectureOfThisCompanyViewController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/08/15.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "LectureOfThisCompanyViewController.h"

@interface LectureOfThisCompanyViewController ()

@end

@implementation LectureOfThisCompanyViewController
NSMutableArray *lists,*subs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Lectures of this company";
    
    lists = [[NSMutableArray alloc]init];
    [lists addObject:@"About My Company"];
    [lists addObject:@"How to get your job"];
    
    
    subs = [[NSMutableArray alloc]init];
    [subs addObject:@"car"];
    [subs addObject:@"IT"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [lists count];
    //return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"lectureListsOfCompany";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = lists[indexPath.row];
    cell.detailTextLabel.text = subs[indexPath.row];
    
    return cell;
}

@end
