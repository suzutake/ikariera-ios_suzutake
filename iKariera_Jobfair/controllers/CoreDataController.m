//
//  CoreDataController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/09.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "CoreDataController.h"

@implementation CoreDataController

/*
 * from design patern book
 */

static CoreDataController*  _sharedInstance = nil;

+ (CoreDataController*)sharedManager
{
    // create Instance
    if (!_sharedInstance) {
        _sharedInstance = [[CoreDataController alloc] init];
    }
    
    return _sharedInstance;
}

- (NSManagedObjectContext*)managedObjectContext
{
    if(_managedObjectContext){
        return _managedObjectContext;
    }
    
    NSManagedObjectModel* managedObjectModel;
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    NSPersistentStoreCoordinator* persistentStoreCoordinator;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:managedObjectModel];
    
    
    //test to get path from AppDelegate
    //http://stackoverflow.com/questions/14591590/how-do-i-reset-the-core-data-stack-after-restoring-sqlite-file-from-backup
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSArray* stores=[[appDelegate persistentStoreCoordinator]persistentStores];
    NSPersistentStore* store = [stores objectAtIndex:0];
    NSURL* storeURL = store.URL;
    NSLog(@"storeURL is %@",storeURL);
    
    NSPersistentStore* persistentStore;
    NSError* error=nil;
    persistentStore = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    if (!persistentStore && error) {
        NSLog(@"Failed to create add persistent store, %@",[error localizedDescription]);
    }    
    
    _managedObjectContext = [[NSManagedObjectContext alloc]init];
    
    [_managedObjectContext setPersistentStoreCoordinator:persistentStoreCoordinator];
    
    return _managedObjectContext;
}

-(Event*)insertNewEventWithDictionary:(NSDictionary*)dictionary
{
    NSString* tablename=@"Event";
    NSArray* nameArray = [dictionary valueForKey:@"name"];
    NSString *jobfairName1 = [nameArray objectAtIndex:0];
    NSLog(@"jobfairName1 is %@",jobfairName1);
    
    NSManagedObjectContext* context;
    context = self.managedObjectContext;
    
    Event* event;
    event = [NSEntityDescription insertNewObjectForEntityForName:tablename
                                          inManagedObjectContext:context];
    event.timeStamp = jobfairName1;
    return event;
}

- (Jobfair*)insertNewJobfairWithDictionary:(NSDictionary*)parsedJSONData
{
    NSString* tablename=@"Jobfair";    
    NSManagedObjectContext* context;
    context = self.managedObjectContext;
    
    Jobfair* jobfair;
    
    for(int i=0;i<[parsedJSONData count];i++)
    {
        // below 1 line is the key. if you dont write the code, data will be update, not insert.
        jobfair = [NSEntityDescription insertNewObjectForEntityForName:tablename
                                                inManagedObjectContext:context];
        jobfair.id = [[[parsedJSONData valueForKey:@"id"]objectAtIndex:i]stringValue];
        jobfair.name = [[parsedJSONData valueForKey:@"name"]objectAtIndex:i];
        jobfair.city = [[parsedJSONData valueForKey:@"city"]objectAtIndex:i];
        
        NSDateFormatter *dateformatter =[[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        jobfair.dateBegin = [dateformatter dateFromString:[[parsedJSONData valueForKey:@"dateBegin"]objectAtIndex:i]];
        NSLog(@"jobfair.dateBegin is %@",jobfair.dateBegin);
        jobfair.dateEnd = [dateformatter dateFromString:[[parsedJSONData valueForKey:@"dateEnd"]objectAtIndex:i]];
        NSLog(@"jobfair.dateEnd is %@",jobfair.dateEnd);
        
        //jobfair.description = [[parsedJSONData valueForKey:@"description"]objectAtIndex:i];
        
        [self save];
    }
    
    return jobfair;
}

-(void)insertTestData
{
    NSManagedObject* newObject;
    newObject = [NSEntityDescription insertNewObjectForEntityForName:@"Jobfair" inManagedObjectContext:self.managedObjectContext];
    [newObject setValue:@"test!" forKey:@"name"];
    
    [self save];
}

- (Company*)insertNewCompanyWithArray:(NSArray*)parsedJSONData
{
    //we can't use stringValue method for string data.
    NSString* tablename=@"Company";
    
    NSManagedObjectContext* context;
    context = self.managedObjectContext;
    
    Company* company;
    for (int j=0;j<[parsedJSONData count];j++)
    {
        for(int i=0;i<[[parsedJSONData objectAtIndex:j] count];i++)
        {
            // below 1 line is the key. if you dont write the code, data will be update, not insert.
            company = [NSEntityDescription insertNewObjectForEntityForName:tablename
                                                inManagedObjectContext:context];
            company.name = [[[parsedJSONData objectAtIndex:j] objectAtIndex:i] valueForKey:@"companyName"];
            company.id = [[[[parsedJSONData objectAtIndex:j] objectAtIndex:i] valueForKey:@"id"]stringValue];
            //guide needs transfer true to "true". now, it's 1 using stringValue
            company.guide=[[[[parsedJSONData objectAtIndex:j] objectAtIndex:i] valueForKey:@"guide"]stringValue];
            company.stand=[[[parsedJSONData objectAtIndex:j] objectAtIndex:i] valueForKey:@"stand"];
            [self save];
        }
    }
    
    return company;
}

-(void)deleteTableDataFromDatabase:(NSString*)tableName
{
    NSManagedObjectContext* context;
    context = self.managedObjectContext;    
    NSFetchRequest* allJobfairs=[[NSFetchRequest alloc]init];
    [allJobfairs setEntity:[NSEntityDescription entityForName:tableName inManagedObjectContext:context]];
    [allJobfairs setIncludesPendingChanges:NO];
    
    NSError* error=nil;
    NSArray* deleteObjects = [context executeFetchRequest:allJobfairs error:&error];
    for(NSManagedObject* delete in deleteObjects)
    {
        [context deleteObject:delete];
    }
    
    [self save];
}

-(NSArray*)sortedCoreData:(NSString*)tablename
{
    NSManagedObjectContext* context;
    context = self.managedObjectContext;
    
    NSFetchRequest* request;
    NSEntityDescription* entity;
    NSSortDescriptor* sortDescriptor;
    request = [[NSFetchRequest alloc]init];
    entity = [NSEntityDescription entityForName:tablename inManagedObjectContext:context];
    [request setEntity:entity];
    sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"id" ascending:YES];
    [request sortDescriptors];
    
    NSArray* result;
    NSError* error = nil;
    result = [context executeFetchRequest:request error:&error];
    if(!result)
    {
        NSLog(@"executeFetchRequest: failed %@",[error localizedDescription]);
        return nil;
    }
    
    return result;
}

- (void)save
{
    // 保存
    NSError*    error;
    if (![self.managedObjectContext save:&error]) {
        // エラー
        NSLog(@"Error, %@", error);
    }
}


/*
 * below are old codes
 */

/*
- (NSFetchedResultsController *)fetchedResultsController:(NSString *)entityName
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Jobfair" inManagedObjectContext:self.managedObjectContext];    
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Jobfair" inManagedObjectContext:context];
    [newManagedObject setValue:@"Jobfair!" forKey:@"name"];
    [newManagedObject setValue:@"1" forKey:@"id"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}
 */

@end
