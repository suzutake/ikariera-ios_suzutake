//
//  ikvMapDetailViewController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/08/15.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "MapDetailViewController.h"

@interface MapDetailViewController ()

@end

@implementation MapDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Map Detail";
    
    NSString* path = @"http://www.ikariera.cz/static/g8qJmoEbMFpY6zThC1XEMVfjSS6HDUGNjOb0cVW9qms.png";
    
    //NSURL* url = [NSURL URLWithString:path];
    
    //NSData* data = [NSData dataWithContentsOfURL:url];
    
    //UIImage* img = [[UIImage alloc] initWithData:data cache:NO];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL: [NSURL URLWithString:path]]];
    self.Image_Map.image=image;	
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
