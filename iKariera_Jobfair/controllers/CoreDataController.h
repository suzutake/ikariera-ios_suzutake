//
//  CoreDataController.h
//  iKariera_Jobfair
//
//  Created by takerukun on 13/09/09.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//


#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Event.h"
#import "Jobfair.h"
#import "Company.h"

@interface CoreDataController : NSObject <NSFetchedResultsControllerDelegate>
{
    NSManagedObjectContext *_managedObjectContext;
}

@property (strong, readonly) NSManagedObjectContext *managedObjectContext;

+ (CoreDataController*)sharedManager;

- (Event*)insertNewEventWithDictionary:(NSDictionary*)dictionary;
- (Jobfair*)insertNewJobfairWithDictionary:(NSDictionary*)parsedJSONData;
- (Company*)insertNewCompanyWithArray:(NSArray*)parsedJSONData;
-(void)deleteTableDataFromDatabase:(NSString*)tableName;

//test
-(void)insertTestData;


- (void)save;

@end
