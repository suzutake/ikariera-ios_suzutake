//
//  LectureListsViewController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/08/26.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "LectureListsViewController.h"

@interface LectureListsViewController ()

@end

@implementation LectureListsViewController
NSMutableArray *lists,*subs;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lists = [[NSMutableArray alloc]init];
    [lists addObject:@"22 good methods"];
    [lists addObject:@"How to get a great job"];
    
    
    subs = [[NSMutableArray alloc]init];
    [subs addObject:@"Czech car inc."];
    [subs addObject:@"Innovation s.r.o"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [lists count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"lectureLists";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = lists[indexPath.row];
    cell.detailTextLabel.text = subs[indexPath.row];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}



@end
