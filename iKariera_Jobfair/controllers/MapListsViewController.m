//
//  MapListsViewController.m
//  iKariera_Jobfair
//
//  Created by takerukun on 13/08/26.
//  Copyright (c) 2013年 cz.senman.ikariera_mobile. All rights reserved.
//

#import "MapListsViewController.h"

@interface MapListsViewController ()

@end

@implementation MapListsViewController
NSMutableArray *lists,*subs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //[self.tableView registerClass: [FontCell class] forCellReuseIdentifier:@"FontCell"];
    
    self.title=@"Map Lists";
    
    
    lists = [[NSMutableArray alloc]init];
    [lists addObject:@"A tower"];
    [lists addObject:@"B tower"];
    
    
    subs = [[NSMutableArray alloc]init];
    [subs addObject:@"main boose"];
    [subs addObject:@"IT boose"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [lists count];
    //return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"map";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    /*
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell== nil){
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    */
     
    
    cell.textLabel.text = lists[indexPath.row];
    cell.detailTextLabel.text = subs[indexPath.row];
    
    return cell;
}

@end
